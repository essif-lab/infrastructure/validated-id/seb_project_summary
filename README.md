# SSI eIDAS Bridge (SEB) project summary

For more information, please contact: 

- [Xavier Vila](mailto:xavi.vila@validated.id)

## Introduction

### About Validated ID

Validated ID was born in 2012 with the purpose of offering a digital signature services with high legal robustness and very easy to use. We developped a set of services to facilitate signing from smartphones, in a centralized way and also using a digitalized handwritten signature. 

Later on, we started building VIDchain, an SSI service for companies to assist them issue, manage and validate Verifiable Credentials.

### SSI eIDAS Bridge

The eIDAS bridge is a component that proposes to enhance the legal certainty of any class of verifiable credential, by incorporating the issuer’s advanced or qualified electronic signature (if the issuer is a natural person) or seal (if the issuer is a legal person).

Basically it allows Issuers to issue credentials that incorporate the issuer’s advanced or qualified electronic signature (if the issuer is a natural person) or seal (if the issuer is a legal person).

## Summary

### Business Problem

Trustworthiness in a Verifiable Credential is linked to the issuer’s DID: Verifying the identity of the issuer is paramount, since there is no binding of a DID to a real-world natural or legal person per se.

SSI eIDAS Bridge pretends to be a **link between existing technology and current regulation**, which do not considers SSI yet. eIDAS regulation will surely evolve to be more inclusive and consider aspects of decentralization (blockchain), but it will still take some time to evolve.

The **main role** of the eIDAS Bridge is thus to assist:
1. **Issuers**, in the process of signing/sealing a verifiable credential, and 
1. **Verifiers**, in the last mile of the verification process, to help identifying the natural or legal person behind an issuer’s DID.

### Technical Solution

We want to focus on providing a library and an API that is able to sign using CAdES and further on JAdES. We propose to use [proof sets](https://w3c-ccg.github.io/ld-proofs/#proof-sets) to accomodate a Linked Data Signature using a new signature suite (e.g. CAdESRSASignature).

The features of the proposed solution are: 
- Assist in setting up the Issuer’s qualified certificate.
- Assist in signing or sealing Verifiable Credentials with the private key of a qualified certificate (QEC).
- Assist in the verification process of a Verifiable Credential to retrieve the Qualified Certificate and verify it against the EU Trusted Lists.

## Integration with the eSSIF-Lab Functional Architecture and Community

We want this component to help other eSSIF-Lab innovators to build and provide solutions that require legal and trust Issuer verification. 

In the reference implementation we want to provide 2 endpoints for eSealing and verification, and one for setting up a QEC to be later used for eSealing.

- The ´signatures´ endpoint creates an eIDAS eSeal signature using the associated EIDAS keys with a DID.
- The ´signature-validations´ Validates a W3C Verifiable Credential eSealed with an EIDAS QEC
- The ´eidas-keys´ endpoint allows any DID to set up an eIDAS QEC to eSeal W3C Verifiable Credentials




