# Goals of SEB development in eSSIF-Lab
- Rewriting the SEB API, more specifically:
  - Add support to new key algorithms and curves: Ed25519, RSA (for signing and validation)
  - Add support to jwt signatures for the previous key algorithms
  - Add Support to lds-signatures for ES256K plus the previous ones
  - Support external configuration data storage
  - Mock a local P12 import on Typescript project
- Development of PKCS11 driver to integrate with HSMs, or external cloud key storages
- Integration of bridge with remote digital signature services
